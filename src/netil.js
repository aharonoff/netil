var keys = {
  up : 87,
  down : 83,
  left : 65,
  right : 68,
  next : 69,
  previous : 81,
  rotate : 32,
  place : 13,
  quit : 27,
  enter : 13,
  restart : 82,
}
var bground = {
  center : null,
  border : null,
  size : null,
  board : null,
  weight : null,
  form : null,
  level : null,
}
var distance = {
  bground : null,
  levels : null,
  board : null,
  form : null,
}
var tile = {
  levels : null,
  board : null,
  form : null,
  onboard : null,
}
var states = {
  menu : true,
  levels : false,
  about : false,
  play : false,
  solved : [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]],
  done : null,
}
var gamer = {
  x : 0,
  y : 0,
}
var sounds = {
  bool : true,
  move : null,
  skip : null,
  rotate : null,
  place : null,
  enter : null,
  quit : null,
  restart : null,
  fail : null,
  done : null,
  cant : null,
}
var texts = {
  font : null,
  large : null,
  medium : null,
  small : null,
  stroke : null,
  title : 'NETIL',
  menu : ['START', 'ABOUT'],
  credit : 'AHARONOFF 2020',
  about : 'NETIL IS A KIND OF\nBLOCK-DROPPING PUZZLE\nVIDEO GAME.\nTHE OBJECTIVE IS\nTO CLEAR THE BOARD\nBY PLACING FORMS\nIN THE RIGHT PLACES\nBY MOVING AND\nROTATING THEM.',
  restart : 'HIT R TO RESTART\nOR ESC TO QUIT',
  done : 'WELL DONE',
  tutorial : ['HIT Q/E TO SKIP\nAMONG FORMS', 'HIT WASD TO MOVE\nFORMS AROUND', 'HIT SPACE TO\nROTATE FORMS', 'HIT ENTER TO PLACE\nFORMS ON THE BOARD'],
}
var colors = {
  background : [22,19,35],
  empty : [22,19,35],
  tile : [[255,125,25],[255,120,24],[255,115,23],[255,110,22],[255,105,21],[255,100,20],[255,95,19],[255,90,18],[255,85,17],[255,80,16],[255,75,15],[255,70,14],[255,65,13],[255,60,12],[255,55,11],[255,50,10],[255,45,9],[255,40,8],[255,35,7],[255,30,6],[255,25,5],[255,20,4],[255,15,3],[255,10,2],[255,5,1]],
  border : [12,9,25],
  alpha : [0,0.083,0.167,0.25,0.333,0.417,0.5,0.583,0.667,0.75,0.833,0.917,1],
}
var count = {
  item : 0,
  given : [1,2,3,3,4,4,5,5,4,6,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8],
  moves : 0,
  enter : 0,
  tutorial : 0,
}
var levels = {
  levels : [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14],[15,16,17,18,19],[20,21,22,23,24]],
  selected : -1,
  original :
  // 1
  [[[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,1,0,0,0],[0,0,1,1,0,0,0],[0,0,0,1,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 2
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,1,1,0,0,0],[0,0,1,0,1,0,0],[0,0,0,1,1,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 3
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,1,1,1,0,0],[0,0,1,1,1,0,0],[0,0,1,1,1,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 4
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,1,1,0,0],[0,0,1,1,1,1,0],[0,0,0,1,1,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 5
  [[0,0,0,0,0,0,0],[0,1,1,1,1,0,0],[0,1,1,1,1,0,0],[0,1,1,1,1,0,0],[0,1,1,1,1,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 6
  [[0,0,0,0,0,0,0],[0,1,1,0,0,0,0],[0,1,1,1,1,0,0],[0,1,1,0,1,1,0],[0,0,1,1,1,1,0],[0,0,0,0,1,1,0],[0,0,0,0,0,0,0]],
  // 7
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,1,1,1,1,0],[0,0,1,0,0,1,0],[0,0,1,0,0,1,0],[0,0,1,1,1,1,0],[0,0,0,0,0,0,0]],
  // 8
  [[0,0,0,0,0,0,0],[0,0,0,0,1,1,0],[0,0,1,1,1,1,0],[0,0,1,1,1,0,0],[0,1,1,1,1,0,0],[0,1,1,0,0,0,0],[0,0,0,0,0,0,0]],
  // 9
  [[0,0,0,0,0,0,0],[0,1,1,1,0,0,0],[0,1,0,1,1,0,0],[0,1,1,1,0,0,0],[0,1,0,1,1,0,0],[0,1,1,1,0,0,0],[0,0,0,0,0,0,0]],
  // 10
  [[0,0,0,0,0,0,0],[0,0,0,1,1,0,0],[0,0,1,1,1,1,0],[0,0,1,1,1,1,0],[0,1,0,1,1,0,0],[0,0,1,0,0,0,0],[0,0,0,0,0,0,0]],
  // 11
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[1,1,1,0,0,1,0],[0,0,0,1,0,0,0],[0,1,0,0,1,1,1],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 12
  [[0,0,0,1,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[1,0,0,0,0,0,1],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,1,0,0,0]],
  // 13
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,1,0,0,1,1,0],[0,1,0,1,0,1,0],[0,1,0,1,0,1,0],[0,0,1,0,0,1,0],[0,0,0,0,0,0,0]],
  // 14
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,2,2,2,0,2,0],[0,2,1,0,1,0,0],[0,2,2,2,0,2,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 15
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,1,1,1,1,1,0],[0,2,2,1,0,1,0],[0,2,0,2,1,1,0],[0,2,2,2,2,2,0],[0,0,0,0,0,0,0]],
  // 16
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,2,1,1,0,0],[0,1,0,1,1,0,0],[0,0,1,0,2,0,0],[0,0,0,1,0,0,0],[0,0,0,0,0,0,0]],
  // 17
  [[0,0,0,0,0,0,0],[0,0,2,1,2,0,0],[0,2,2,2,2,0,0],[0,1,1,0,2,0,0],[0,2,2,2,2,0,0],[0,0,2,1,2,0,0],[0,0,0,0,0,0,0]],
  // 18
  [[0,0,0,2,0,0,0],[0,0,2,2,2,0,0],[0,0,2,1,2,0,0],[0,0,1,1,1,0,0],[0,0,1,2,1,0,0],[0,0,0,2,0,0,0],[0,0,0,0,0,0,0]],
  // 19
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,1,1,1,2,2,0],[0,2,2,2,2,2,1],[0,1,1,1,2,2,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 20
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,2,2,2,1,0],[0,2,2,1,2,2,2],[0,0,2,2,2,1,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 21
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,2,2,1,1,0],[0,0,2,3,1,1,0],[0,0,2,2,1,1,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 22
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,1,3,1,2,3,0],[0,1,1,2,1,2,0],[0,1,3,1,2,3,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 23
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,3,3,0,2,0,0],[0,2,3,0,3,3,0],[0,3,3,0,2,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 24
  [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,1,1,3,0,0],[0,0,1,3,1,0,0],[0,0,0,1,1,0,0],[0,1,0,0,0,0,0],[0,0,0,0,0,0,0]],
  // 25
  [[0,0,0,0,0,0,0],[0,1,2,2,0,0,0],[0,2,0,3,2,0,0],[0,2,3,3,1,0,0],[0,0,2,1,2,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]],
  final : [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
}
var forms = {
  selected : -1,
  original : [
    [ // 1
      [[[0,1,0],[1,1,1],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 2
      [[[0,1,0],[1,1,0],[0,0,0]], [[0,1,0],[1,1,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 3
      [[[0,1,1],[0,0,1],[0,0,1]], [[0,1,1],[0,0,1],[0,0,1]], [[0,0,0],[0,1,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 4
      [[[0,1,0],[1,0,0],[0,0,1]], [[0,1,0],[1,0,0],[0,0,1]], [[0,1,0],[0,1,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 5
      [[[0,1,0],[0,1,1],[0,1,0]], [[0,1,0],[0,1,1],[0,1,0]], [[0,1,0],[0,1,1],[0,1,0]]], [[[0,1,0],[0,1,1],[0,1,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 6
      [[[0,1,0],[0,1,0],[0,1,1]], [[0,1,0],[0,1,0],[0,1,1]], [[0,1,0],[0,1,0],[1,1,0]]], [[[0,1,0],[0,1,0],[1,1,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 7
      [[[0,1,0],[1,1,0],[0,1,0]], [[0,1,0],[1,1,0],[0,1,0]], [[0,1,0],[1,1,0],[0,1,0]]], [[[0,1,0],[1,1,0],[0,1,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,1,1],[0,1,1],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 8
      [[[1,1,0],[0,1,0],[0,1,1]], [[1,1,0],[0,1,0],[0,1,1]], [[0,1,1],[0,1,1],[0,0,0]]], [[[0,1,1],[0,1,1],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[1,0,0],[0,1,0],[0,0,1]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 9
      [[[1,0,1],[0,0,0],[1,0,1]], [[0,1,0],[1,1,1],[0,1,0]], [[0,1,1],[0,1,0],[0,1,1]]], [[[0,1,1],[0,1,0],[0,1,1]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]], [[[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 10
      [[[0,1,0],[0,1,0],[0,1,0]], [[0,1,0],[0,1,0],[0,1,0]], [[1,0,1],[0,1,0],[1,0,1]]], [[[1,1,0],[0,1,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[1,1,0],[0,1,0],[0,0,0]]], [[[0,0,1],[0,1,0],[1,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 11
      [[[0,1,0],[0,1,1],[0,1,0]], [[0,1,0],[0,1,0],[0,1,0]], [[0,1,0],[0,1,1],[0,1,0]]], [[[1,0,0],[0,1,0],[0,0,1]], [[0,0,0],[0,0,0],[0,0,0]], [[1,0,0],[0,1,0],[0,0,1]]], [[[1,0,0],[0,1,0],[0,0,1]], [[1,0,0],[0,1,0],[0,0,1]], [[0,0,0],[0,0,0],[0,0,0]]]
    ],
    [ // 12
      [[[0,1,0],[1,0,0],[0,1,0]], [[0,1,0],[1,0,0],[0,1,0]], [[0,1,0],[1,0,0],[0,1,0]]], [[[0,1,0],[1,0,0],[0,1,0]], [[0,0,0],[0,0,0],[0,0,0]], [[1,0,0],[0,0,1],[0,1,0]]], [[[1,0,0],[0,0,1],[0,1,0]], [[1,0,0],[0,0,1],[0,1,0]], [[1,0,0],[0,0,1],[0,1,0]]]
    ],
    [ // 13
      [[[1,0,0],[0,0,1],[0,1,0]], [[1,0,0],[0,0,1],[0,1,0]], [[1,0,0],[0,0,1],[0,1,0]]], [[[1,0,0],[0,0,1],[1,1,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,1,0],[1,1,0],[0,0,0]]], [[[0,1,0],[1,1,0],[0,0,0]], [[0,1,0],[0,1,0],[1,1,0]], [[0,1,0],[0,1,0],[1,1,0]]]
    ],
    [ // 14
      [[[0,2,0],[0,2,0],[0,2,0]], [[0,2,0],[0,2,0],[0,2,0]], [[0,0,2],[2,2,0],[0,0,2]]], [[[0,0,2],[2,2,0],[0,0,2]], [[0,0,0],[0,0,0],[0,0,0]], [[0,2,0],[2,0,2],[0,2,0]]], [[[2,0,2],[0,0,0],[2,0,2]], [[0,0,1],[0,1,0],[1,0,0]], [[0,0,1],[0,1,0],[1,0,0]]]
    ],
    [ // 15
      [[[0,2,0],[0,0,2],[2,2,0]], [[0,1,0],[0,0,1],[1,1,0]], [[2,2,2],[2,0,2],[0,0,0]]], [[[1,1,1],[1,0,1],[0,0,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,1,0],[0,1,1],[0,1,0]]], [[[2,2,0],[0,2,0],[0,2,0]], [[0,2,0],[0,2,2],[0,2,0]], [[1,1,0],[0,1,0],[0,1,0]]]
    ],
    [ // 16
      [[[0,1,0],[1,0,0],[0,1,0]], [[0,0,1],[2,0,1],[0,0,1]], [[2,0,0],[0,1,0],[2,0,0]]], [[[2,1,0],[0,1,1],[0,0,2]], [[0,0,0],[0,0,0],[0,0,0]], [[1,2,0],[0,2,2],[0,0,1]]], [[[0,1,0],[0,2,1],[0,1,0]], [[0,2,0],[0,2,0],[0,2,0]], [[0,0,0],[2,1,2],[0,0,0]]]
    ],
    [ // 17
      [[[0,2,0],[0,2,0],[0,2,2]], [[1,2,0],[0,1,0],[0,1,0]], [[0,2,0],[0,1,1],[0,2,0]]], [[[0,2,0],[0,0,0],[0,0,1]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,2],[0,1,1],[0,2,0]]], [[[0,2,0],[1,0,1],[0,1,0]], [[0,1,0],[0,1,1],[0,2,0]], [[0,2,2],[0,2,1],[0,0,0]]]
    ],
    [ // 18
      [[[1,0,1],[0,2,0],[0,2,0]], [[0,2,0],[1,0,1],[0,0,0]], [[0,2,0],[2,2,0],[0,2,0]]], [[[2,0,0],[0,0,2],[0,1,0]], [[0,0,0],[0,0,0],[0,0,0]], [[1,0,0],[0,0,2],[0,2,0]]], [[[2,0,2],[0,2,0],[0,1,0]], [[0,1,0],[1,0,2],[0,2,0]], [[0,1,0],[1,2,0],[0,1,0]]]
    ],
    [ // 19
      [[[0,1,2],[0,2,0],[0,1,2]], [[1,2,0],[0,0,0],[2,1,0]], [[0,2,0],[0,2,0],[0,1,1]]], [[[0,2,0],[1,2,0],[0,2,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,1,0],[0,2,0],[0,1,0]]], [[[0,1,1],[0,2,0],[1,1,0]], [[0,1,0],[0,2,0],[2,1,0]], [[0,1,1],[0,0,0],[2,2,0]]]
    ],
    [ // 20
      [[[0,1,2],[2,1,0],[0,0,0]], [[0,1,1],[2,2,0],[0,1,1]], [[0,1,0],[2,2,0],[0,0,0]]], [[[0,2,0],[0,2,1],[0,2,0]], [[0,0,0],[0,0,0],[0,0,0]], [[2,1,0],[0,2,1],[0,0,0]]], [[[0,1,0],[0,0,2],[0,0,0]], [[0,1,2],[0,0,1],[0,1,2]], [[0,2,0],[2,0,2],[0,1,0]]]
    ],
    [ // 21
      [[[0,1,0],[3,1,0],[0,1,0]], [[0,2,0],[1,2,0],[0,2,0]], [[0,3,0],[0,2,0],[0,1,0]]], [[[3,3,0],[0,2,0],[0,2,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,1,0],[0,1,0],[2,2,0]]], [[[0,2,0],[2,0,3],[0,3,0]], [[1,1,3],[0,0,2],[0,0,2]], [[0,3,0],[0,2,0],[2,2,0]]]
    ],
    [ // 22
      [[[3,3,0],[0,2,0],[3,3,0]], [[1,1,0],[0,3,0],[0,1,1]], [[0,1,0],[2,2,0],[0,1,0]]], [[[3,0,1],[0,0,1],[3,0,1]], [[0,0,0],[0,0,0],[0,0,0]], [[2,2,0],[0,3,0],[0,2,2]]], [[[0,3,0],[3,3,0],[0,0,0]], [[0,2,0],[2,1,0],[0,0,0]], [[0,3,0],[0,3,0],[2,0,2]]]
    ],
    [ // 23
      [[[0,3,0],[0,3,3],[0,3,0]], [[3,0,0],[0,0,2],[0,1,0]], [[3,0,0],[0,0,2],[0,1,0]]], [[[0,2,0],[1,0,3],[0,2,0]], [[0,0,0],[0,0,0],[0,0,0]], [[2,0,2],[0,1,0],[0,1,0]]], [[[0,3,0],[2,0,2],[1,0,1]], [[0,1,0],[0,2,3],[0,1,0]], [[0,2,0],[3,0,2],[0,2,0]]]
    ],
    [ // 24
      [[[0,1,0],[0,1,0],[0,2,3]], [[0,3,0],[0,2,0],[0,1,1]], [[0,3,0],[0,2,1],[0,3,0]]], [[[0,3,0],[2,0,0],[0,3,0]], [[0,0,0],[0,0,0],[0,0,0]], [[0,0,3],[0,0,0],[1,0,0]]], [[[0,0,2],[0,0,0],[3,0,0]], [[1,0,0],[0,3,2],[0,2,0]], [[0,1,2],[1,0,0],[2,0,3]]]
    ],
    [ // 25
      [[[1,0,2],[0,3,0],[2,0,0]], [[0,3,0],[3,0,2],[0,2,1]], [[3,0,2],[0,1,0],[2,0,0]]], [[[0,1,0],[1,0,2],[0,2,3]], [[0,0,0],[0,0,0],[0,0,0]], [[1,0,0],[0,0,2],[0,3,0]]], [[[3,0,0],[0,0,2],[0,1,0]], [[0,3,2],[3,0,0],[2,0,0]], [[0,1,2],[1,0,0],[0,0,0]]]
    ],
  ],
}
var start = JSON.parse(JSON.stringify(levels.original))
var form = JSON.parse(JSON.stringify(forms.original))

function preload(){
  texts.font = loadFont('font/slkscr.ttf')
  sounds.move = loadSound('sound/move.wav')
  sounds.skip = loadSound('sound/skip.wav')
  sounds.rotate = loadSound('sound/rotate.wav')
  sounds.place = loadSound('sound/place.wav')
  sounds.enter = loadSound('sound/enter.wav')
  sounds.quit = loadSound('sound/quit.wav')
  sounds.restart = loadSound('sound/restart.wav')
  sounds.fail = loadSound('sound/fail.wav')
  sounds.done = loadSound('sound/done.wav')
  sounds.cant = loadSound('sound/cant.wav')
}

function setup(){
  createCanvas(windowWidth, windowHeight)
  bground.center = round(windowHeight * 0.75 * 0.5)
  bground.border = round(windowHeight * 0.75)
  bground.size = round(windowHeight * 0.75 * 0.925)
  bground.board = round(windowHeight * 0.75 * 0.625)
  bground.weight = round(windowHeight * 0.75 * 0.025)
  bground.form = round(windowHeight * 0.75 * 0.12)
  bground.levels = round(windowHeight * 0.75 * 0.5875)
  distance.bground = round(windowHeight * 0.75 * 0.08 * 4.9)
  distance.levels = round(windowHeight * 0.75 * 0.015 * 2)
  distance.board = round(windowHeight * 0.75 * 0.015)
  distance.form = round(windowHeight * 0.75 * 0.08 * 0.325)
  tile.levels = round(windowHeight * 0.75 * 0.065 * 2)
  tile.board = round(windowHeight * 0.75 * 0.065)
  tile.form = round(windowHeight * 0.75 * 0.065 * 0.325)
  tile.onboard = round(windowHeight * 0.75 * 0.065 * 0.325 * 2)
  texts.large = round(windowHeight * 0.75 * 0.25)
  texts.medium = round(windowHeight * 0.75 * 0.125)
  texts.small = round(windowHeight * 0.75 * 0.0625)
  texts.stroke = round(windowHeight * 0.75 * 0.125)
}

function draw(){
  createCanvas(bground.border, bground.border)
  background(colors.background)
  rectMode(CENTER)
  frameRate(60)
  colorMode(RGB, 255, 255, 255, 1)
  textFont(texts.font)
  textAlign(CENTER, CENTER)
  // border of window
  noFill()
  stroke(colors.border)
  strokeWeight(bground.weight)
  rect(bground.center, bground.center, bground.border, bground.border)
  // menu
  if(states.menu === true && states.levels === false && states.about === false && states.play === false){
    stroke(colors.border)
    strokeWeight(texts.stroke)
    textSize(texts.large)
    fill(colors.tile[0])
    text(texts.title, bground.center, bground.border * 0.2)
    for(var i = 0; i < colors.alpha.length - 1; i++){
      stroke(colors.tile[0][0],colors.tile[0][1],colors.tile[0][2],colors.alpha[i])
      strokeWeight(texts.stroke - texts.stroke * 0.25 * i)
      textSize(texts.large)
      fill(colors.tile[0])
      text(texts.title, bground.center, bground.border * 0.2)
    }
    for(var i = 0; i < texts.menu.length; i++){
      fill(colors.tile[0][0],colors.tile[0][1],colors.tile[0][2],colors.alpha[6])
      stroke(colors.border)
      strokeWeight(texts.stroke * 0.4)
      textSize(texts.medium)
      text(texts.menu[i], bground.center, bground.border * 0.5 + i * bground.border * 0.15)
      for(var j = 0; j < texts.menu.length; j++){
        if(count.item === j){
          fill(colors.tile[0])
          noStroke()
          text(texts.menu[j], bground.center, bground.border * 0.5 + j * bground.border * 0.15)
        }
      }
    }
    fill(colors.tile[0][0],colors.tile[0][1],colors.tile[0][2],colors.alpha[3])
    fill(colors.tile[0])
    noStroke()
    textSize(texts.small)
    text(texts.credit, bground.center, bground.border * 0.9)
    if(mouseX >= (bground.center * 1.5 + bground.weight) * 0.5 && mouseX <= bground.border - (bground.center * 1.5 + bground.weight) * 0.5 && mouseY >= (bground.center * 0.5 + bground.weight) * 0.5 && mouseY <= bground.border - (bground.center * 0.5 + bground.weight) * 0.5){
      fill(colors.background)
      stroke(colors.border)
      strokeWeight(bground.weight)
      rect(mouseX, mouseY, bground.center * 1.5, bground.center * 0.5)
      fill(colors.tile[0])
      stroke(colors.border)
      strokeWeight(bground.weight)
      text('USE WASD, Q, E,\nSPACE, ENTER, ESC', mouseX, mouseY)
    }
  }
  // levels
  if(states.menu === false && states.levels === true && states.about === false && states.play === false){
    fill(colors.border)
    stroke(colors.border)
    strokeWeight(bground.weight * 0.5)
    rect(bground.center, bground.center, bground.size, bground.size)
    for(var i = 0; i < levels.levels.length; i++){
      for(var j = 0; j < levels.levels[i].length; j++){
        if(states.solved[i][j] === 0){
          fill(colors.background)
          noStroke()
          rect(bground.center + (i - 2) * (tile.levels + distance.levels), bground.center + (j - 2) * (tile.levels + distance.levels), tile.levels, tile.levels)
          fill(colors.tile[0])
          noStroke()
          textSize(texts.small)
          text(j * levels.levels.length + i + 1, bground.center + (i - 2) * (tile.levels + distance.levels), bground.center + (j - 2) * (tile.levels + distance.levels))
        } else {
          fill(colors.tile[0][0],colors.tile[0][1],colors.tile[0][2],colors.alpha[6])
          noStroke()
          rect(bground.center + (i - 2) * (tile.levels + distance.levels), bground.center + (j - 2) * (tile.levels + distance.levels), tile.levels, tile.levels)
          fill(colors.tile[0])
          noStroke()
          textSize(texts.small)
          text(j * levels.levels.length + i + 1, bground.center + (i - 2) * (tile.levels + distance.levels), bground.center + (j - 2) * (tile.levels + distance.levels))
        }
      }
    }
    noFill()
    stroke(colors.tile[0])
    rect(bground.center + (gamer.x - 2) * (tile.levels + distance.levels), bground.center + (gamer.y - 2) * (tile.levels + distance.levels), tile.levels, tile.levels)
  }
  // about
  if(states.menu === false && states.levels === false && states.about === true && states.play === false){
    fill(colors.border)
    stroke(colors.border)
    strokeWeight(bground.weight * 0.5)
    rect(bground.center, bground.center, bground.size, bground.size)
    fill(colors.tile[0])
    noStroke()
    textSize(texts.small)
    text(texts.about, bground.center, bground.center)
  }
  // play
  if(states.menu === false && states.levels === false && states.about === false && states.play === true){
    // background of board and forms
    fill(colors.border)
    stroke(colors.border)
    strokeWeight(bground.weight * 0.5)
    rect(bground.center, bground.center, bground.size, bground.size)
    // draw background elements
    // background of forms
    fill(colors.background)
    stroke(colors.background)
    strokeWeight(bground.weight * 0.25)
    for(var i = 0; i < 3; i++){
      for(var j = 0; j < 3; j++){
        rect(bground.center + (i - 1) * distance.bground, bground.center + (j - 1) * distance.bground, bground.form, bground.form)
      }
    }
    // background of board
    fill(colors.background)
    noStroke()
    rect(bground.center, bground.center, bground.board, bground.board)
    fill(colors.border)
    noStroke()
    rect(bground.center, bground.center, bground.levels, bground.levels)

    // draw forms
    for(var k = 0; k < form[levels.selected].length; k++){
      for(var l = 0; l < form[levels.selected][k].length; l++){
        for(var m = 0; m < form[levels.selected][k][l].length; m++){
          for(var n = 0; n < form[levels.selected][k][l][m].length; n++){
            if(levels.selected <= 12){
              if(form[levels.selected][k][l][m][n] === 0){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[0])
              }
              if(form[levels.selected][k][l][m][n] === 1){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[12])
              }
            }
            if(levels.selected > 12 && levels.selected <= 19){
              if(form[levels.selected][k][l][m][n] === 0){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[0])
              }
              if(form[levels.selected][k][l][m][n] === 1){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[6])
              }
              if(form[levels.selected][k][l][m][n] === 2){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[12])
              }
            }
            if(levels.selected > 19){
              if(form[levels.selected][k][l][m][n] === 0){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[0])
              }
              if(form[levels.selected][k][l][m][n] === 1){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[4])
              }
              if(form[levels.selected][k][l][m][n] === 2){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[8])
              }
              if(form[levels.selected][k][l][m][n] === 3){
                fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[12])
              }
            }
            noStroke()
            rect(bground.center + (k - 1) * distance.bground + (m - 1) * distance.form, bground.center + (l - 1) * distance.bground + (n - 1) * distance.form, tile.form, tile.form)
          }
        }
      }
    }
    noFill()
    stroke(colors.tile[levels.selected])
    rect(bground.center + (forms.selected % 3 - 1) * distance.bground, bground.center + floor(forms.selected / 3 - 1) * distance.bground, bground.form, bground.form)

    // draw state
    for(var i = 0; i < start[levels.selected].length; i++){
      for(var j = 0; j < start[levels.selected][i].length; j++){
        if(levels.selected <= 12){
          if(start[levels.selected][i][j] === 0){
            fill(colors.background)
          }
          if(start[levels.selected][i][j] === 1){
            fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[12])
          }
        }
        if(levels.selected > 12 && levels.selected <= 19){
          if(start[levels.selected][i][j] === 0){
            fill(colors.background)
          }
          if(start[levels.selected][i][j] === 1){
            fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[6])
          }
          if(start[levels.selected][i][j] === 2){
            fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[12])
          }
        }
        if(levels.selected > 19){
          if(start[levels.selected][i][j] === 0){
            fill(colors.background)
          }
          if(start[levels.selected][i][j] === 1){
            fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[4])
          }
          if(start[levels.selected][i][j] === 2){
            fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[8])
          }
          if(start[levels.selected][i][j] === 3){
            fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[12])
          }
        }
        noStroke()
        rect(bground.center + (i - 3) * (tile.board + distance.board), bground.center +  (j - 3) * (tile.board + distance.board), tile.board, tile.board)
      }
    }
    // draw form on board
    if(forms.selected > -1){
      for(var i = 0; i < form[levels.selected][forms.selected % 3][floor(forms.selected / 3)].length; i++){
        for(var j = 0; j < form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i].length; j++){
          if(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j] !== 0){
            fill(colors.background)
            noStroke()
            rect(bground.center + (i - 3 + gamer.x) * (tile.board + distance.board), bground.center +  (j - 3 + gamer.y) * (tile.board + distance.board), tile.onboard * (1 + 0.1 * sin(frameCount * 0.1)), tile.onboard * (1 + 0.1 * sin(frameCount * 0.1)))
            if(levels.selected <= 12){
              fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[(2 + (start[levels.selected][i + gamer.x][j + gamer.y] - form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j])) % 2 * 12])
            }
            if(levels.selected > 12 && levels.selected <= 19){
              fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[(3 + (start[levels.selected][i + gamer.x][j + gamer.y] - form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j])) % 3 * 6])
            }
            if(levels.selected > 19){
              fill(colors.tile[levels.selected][0], colors.tile[levels.selected][1], colors.tile[levels.selected][2], colors.alpha[(4 + (start[levels.selected][i + gamer.x][j + gamer.y] - form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j])) % 4 * 4])
            }
            noStroke()
            rect(bground.center + (i - 3 + gamer.x) * (tile.board + distance.board), bground.center +  (j - 3 + gamer.y) * (tile.board + distance.board), tile.onboard * (1 + 0.1 * sin(frameCount * 0.1)), tile.onboard * (1 + 0.1 * sin(frameCount * 0.1)))
          }
        }
      }
    }
    // done text
    if(states.done === true){
      if(frameCount % 32 < 16){
        fill(colors.border)
        stroke(colors.tile[0])
        rect(bground.center, bground.center, tile.levels * texts.done.length * 0.45, tile.levels * 1.5)
        fill(colors.tile[0])
        noStroke()
        textSize(texts.small)
        text(texts.done, bground.center, bground.center)
      }
    }
    // restart text
    if(states.done === false){
      if(frameCount % 128 < 64){
        fill(colors.border)
        stroke(colors.tile[0])
        rect(bground.center, bground.center, tile.levels * texts.restart.length * 0.18, tile.levels * 2)
        fill(colors.tile[0])
        noStroke()
        textSize(texts.small)
        text(texts.restart, bground.center, bground.center)
      }
    }
    // tutorial
    if(levels.selected === 0 && states.done !== false){
      for(var i = 0; i < texts.tutorial.length; i++){
        if(i === count.tutorial && texts.tutorial[i].length !== 0){
          fill(colors.border)
          stroke(colors.tile[0])
          rect(bground.center, bground.border * 0.85, tile.levels * texts.tutorial[i].length * 0.18, tile.levels * 2)
          fill(colors.tile[0])
          noStroke()
          textSize(texts.small)
          text(texts.tutorial[i], bground.center, bground.border * 0.85)
        }
      }
    }
  }
}

function keyPressed(){
  // menu
  if(states.menu === true && states.levels === false && states.about === false && states.play === false){
    if(keyCode === keys.up || keyCode === keys.right){
      count.item++
      if(count.item > texts.menu.length - 1){
        count.item = 0
      }
      playSound(sounds.move)
    }
    if(keyCode === keys.down || keyCode === keys.left){
      count.item--
      if(count.item < 0){
        count.item = texts.menu.length - 1
      }
      playSound(sounds.move)
    }
    if(keyCode === keys.enter){
      if(count.item === 0){
        states.menu = false
        states.levels = true
        states.about = false
        states.play = false
        setTimeout(function enterDelay(){count.enter = 1}, 1)
        playSound(sounds.enter)
      }
      if(count.item === 1){
        states.menu = false
        states.levels = false
        states.about = true
        states.play = false
        playSound(sounds.enter)
      }
    }
  }
  // levels
  if(states.menu === false && states.levels === true && states.about === false && states.play === false){
    if(keyCode === keys.quit){
      states.menu = true
      states.levels = false
      states.about = false
      states.play = false
      count.enter = 0
      playSound(sounds.quit)
    }
    if(keyCode === keys.up){
      gamer.y--
      if(gamer.y < 0){
        gamer.y = 0
        playSound(sounds.cant)
      }
      playSound(sounds.move)
    }
    if(keyCode === keys.down){
      gamer.y++
      if(gamer.y > levels.levels.length - 1){
        gamer.y = levels.levels.length - 1
        playSound(sounds.cant)
      }
      playSound(sounds.move)
    }
    if(keyCode === keys.left){
      gamer.x--
      if(gamer.x < 0){
        gamer.x = 0
        playSound(sounds.cant)
      }
      playSound(sounds.move)
    }
    if(keyCode === keys.right){
      gamer.x++
      if(gamer.x > levels.levels.length - 1){
        gamer.x = levels.levels.length - 1
        playSound(sounds.cant)
      }
      playSound(sounds.move)
    }
    if(keyCode === keys.enter && count.enter === 1){
      states.menu = false
      states.levels = false
      states.about = false
      states.play = true
      forms.selected = -1
      count.moves = 0
      levels.selected = levels.levels[gamer.y][gamer.x]
      start = JSON.parse(JSON.stringify(levels.original))
      form = JSON.parse(JSON.stringify(forms.original))
      playSound(sounds.enter)
    }
  }
  // about
  if(states.menu === false && states.levels === false && states.about === true && states.play === false){
    if(keyCode === keys.quit){
      states.menu = true
      states.levels = false
      states.about = false
      states.play = false
      playSound(sounds.quit)
    }
  }
  // play
  if(states.menu === false && states.levels === false && states.about === false && states.play === true){
    // skip to next form
    if(keyCode === keys.next){
      forms.selected++
      if(forms.selected > 8){
        forms.selected =  0
      }
      if(forms.selected === 4){
        forms.selected++
      }
      for(var i = 0; i < 9; i++){
        if(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)].flat().every(checkArray) === true){
          forms.selected++
          if(forms.selected > 8){
            forms.selected = 0
          }
        }
      }
      gamer.x = 0
      gamer.y = 0
      playSound(sounds.skip)
    }
    // skip to previous form
    if(keyCode === keys.previous){
      forms.selected--
      if(forms.selected < 0){
        forms.selected = 8
      }
      if(forms.selected === 4){
        forms.selected--
      }
      for(var i = 0; i < 9; i++){
        if(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)].flat().every(checkArray) === true){
          forms.selected--
          if(forms.selected < 0){
            forms.selected = 8
          }
        }
      }
      gamer.x = 0
      gamer.y = 0
      playSound(sounds.skip)
    }
    if(forms.selected !== -1){
      // moving form
      if(keyCode === keys.up){
        gamer.y--
        if(gamer.y < -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[0]){
          gamer.y = -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[0]
          playSound(sounds.cant)
        }
        playSound(sounds.move)
      }
      if(keyCode === keys.down){
        gamer.y++
        if(gamer.y > levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[3]){
          gamer.y = levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[3]
          playSound(sounds.cant)
        }
        playSound(sounds.move)
      }
      if(keyCode === keys.left){
        gamer.x--
        if(gamer.x < -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[1]){
          gamer.x = -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[1]
          playSound(sounds.cant)
        }
        playSound(sounds.move)
      }
      if(keyCode === keys.right){
        gamer.x++
        if(gamer.x > levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[2]){
          gamer.x = levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[2]
          playSound(sounds.cant)
        }
        playSound(sounds.move)
      }
      // rotate form
      if(keyCode === keys.rotate){
        form[levels.selected][forms.selected % 3][floor(forms.selected / 3)] = rotate2DArray(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])
        if(gamer.y < -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[0]){
          gamer.y = -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[0]
        }
        if(gamer.y > levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[3]){
          gamer.y = levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[3]
        }
        if(gamer.x < -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[1]){
          gamer.x = -boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[1]
        }
        if(gamer.x > levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[2]){
          gamer.x = levels.final.length - 1 - boundary(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)])[2]
        }
        playSound(sounds.rotate)
      }
      // place form
      if(keyCode === keys.place){
        for(var i = 0; i < form[levels.selected][forms.selected % 3][floor(forms.selected / 3)].length; i++){
          for(var j = 0; j < form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i].length; j++){
            if(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j] !== 0){
              if(levels.selected <= 12){
                start[levels.selected][i + gamer.x][j + gamer.y] = (2 + (start[levels.selected][i + gamer.x][j + gamer.y] - form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j])) % 2
              }
              if(levels.selected > 12 && levels.selected <= 19){
                start[levels.selected][i + gamer.x][j + gamer.y] = (3 + (start[levels.selected][i + gamer.x][j + gamer.y] - form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j])) % 3
              }
              if(levels.selected > 19){
                start[levels.selected][i + gamer.x][j + gamer.y] = (4 + (start[levels.selected][i + gamer.x][j + gamer.y] - form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j])) % 4
              }
              form[levels.selected][forms.selected % 3][floor(forms.selected / 3)][i][j] = 0
            }
          }
        }
        for(var i = 0; i < 9; i++){
          if(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)].flat().every(checkArray) === true){
            forms.selected++
            if(forms.selected > 8){
              forms.selected = 0
            }
          }
        }
        if(form[levels.selected][forms.selected % 3][floor(forms.selected / 3)].flat().every(checkArray) === false || count.moves === count.given[levels.selected] - 1){
          count.moves++
        } else {
        }
        if(equalArray(start[levels.selected], levels.final) === true){
          states.done = true
          setTimeout(function(){states.menu = false; states.levels = true; states.about = false; states.play = false; count.enter = 1; states.done = null;
            for(var i = 0; i < levels.levels.length; i++){
              for(var j = 0; j < levels.levels[i].length; j++){
                if(levels.levels[i][j] === levels.selected){
                  gamer.x = j
                  gamer.y = i
                  states.solved[j][i] = 1
                }
              }
            }
          }, 2048)
          playSound(sounds.done)
        }
        if(equalArray(start[levels.selected], levels.final) === false && count.given[levels.selected] <= count.moves){
          playSound(sounds.fail)
          states.done = false
        } else {
          playSound(sounds.place)
        }
        gamer.x = 0
        gamer.y = 0
      }
    }
    if(equalArray(start[levels.selected], levels.final) === false && count.given[levels.selected] <= count.moves){
      if(keyCode === keys.up || keyCode === keys.down || keyCode === keys.left || keyCode === keys.right || keyCode === keys.place || keyCode === keys.next || keyCode === keys.previous || keyCode === keys.rotate || keys.quit){
        playSound(sounds.fail)
      }
    }
    // restart
    if(keyCode === keys.restart){
      playSound(sounds.fail)
      playSound(sounds.restart)
      gamer.x = 0
      gamer.y = 0
      count.moves = 0
      forms.selected = 0
      states.done = null
      start = JSON.parse(JSON.stringify(levels.original))
      form = JSON.parse(JSON.stringify(forms.original))
    }
    // quit
    if(keyCode === keys.quit){
      states.menu = false
      states.levels = true
      states.about = false
      states.play = false
      count.enter = 1
      states.done = null
      for(var i = 0; i < levels.levels.length; i++){
        for(var j = 0; j < levels.levels[i].length; j++){
          if(levels.levels[i][j] === levels.selected){
            gamer.x = j
            gamer.y = i
          }
        }
      }
      count.moves = 0
      playSound(sounds.quit)
    }
    // tutorial
    if(levels.selected === 0){
      if(keyCode === keys.next || keyCode === keys.previous){
        if(count.tutorial === 0){
          count.tutorial++
        }
      }
      if(keyCode === keys.up || keyCode === keys.down || keyCode === keys.left || keyCode === keys.right){
        if(count.tutorial === 1){
          count.tutorial++
        }
      }
      if(keyCode === keys.rotate){
        if(count.tutorial === 2){
          count.tutorial++
        }
      }
      if(keyCode === keys.place){
        if(count.tutorial === 3){
          count.tutorial++
        }
      }
    }
  }
  muteSound()
}

function boundary(array){
  var topmost = -1
  var leftmost = -1
  var bottommost = -1
  var rightmost = -1
  for(var i = 0; i < array.length; i++){
    for(var j = 0; j < array[i].length; j++){
      if(array[i][j] !== 0){
        rightmost = i
      }
    }
  }
  for(var i = array.length - 1; i >= 0; i--){
    for(var j = array[i].length - 1; j >= 0; j--){
      if(array[i][j] !== 0){
        leftmost = i
      }
    }
  }
  array = rotate2DArray(array)
  for(var i = 0; i < array.length; i++){
    for(var j = 0; j < array[i].length; j++){
      if(array[i][j] !== 0){
        bottommost = i
      }
    }
  }
  for(var i = array.length - 1; i >= 0; i--){
    for(var j = array[i].length - 1; j >= 0; j--){
      if(array[i][j] !== 0){
        topmost = i
      }
    }
  }
  return [topmost, leftmost, rightmost, bottommost]
}
function rotate2DArray(array){
  var result = []
  for(var i = 0; i < array[0].length; i++) {
    var row = array.map(e => e[i]).reverse()
    result.push(row)
  }
  return result
}
function checkArray(a){
  return a < 1
}
function equalArray(arr1,arr2){
  if(arr1.toString() === arr2.toString()){
    return true
  } else {
    return false
  }
}
function playSound(s){
  if(sounds.bool === true){
    s.play()
  }
}
function muteSound(){
  if(keyCode === 77){
    if(sounds.bool === true){
      sounds.bool = false
    } else {
      sounds.bool = true
    }
  }
}
function windowResized(){
  resizeCanvas(windowWidth, windowHeight)
  bground.center = round(windowHeight * 0.75 * 0.5)
  bground.border = round(windowHeight * 0.75)
  bground.size = round(windowHeight * 0.75 * 0.925)
  bground.board = round(windowHeight * 0.75 * 0.625)
  bground.weight = round(windowHeight * 0.75 * 0.025)
  bground.form = round(windowHeight * 0.75 * 0.12)
  bground.levels = round(windowHeight * 0.75 * 0.5875)
  distance.bground = round(windowHeight * 0.75 * 0.08 * 4.9)
  distance.levels = round(windowHeight * 0.75 * 0.015 * 2)
  distance.board = round(windowHeight * 0.75 * 0.015)
  distance.form = round(windowHeight * 0.75 * 0.08 * 0.325)
  tile.levels = round(windowHeight * 0.75 * 0.065 * 2)
  tile.board = round(windowHeight * 0.75 * 0.065)
  tile.form = round(windowHeight * 0.75 * 0.065 * 0.325)
  tile.onboard = round(windowHeight * 0.75 * 0.065 * 0.325 * 2)
  texts.large = round(windowHeight * 0.75 * 0.25)
  texts.medium = round(windowHeight * 0.75 * 0.125)
  texts.small = round(windowHeight * 0.75 * 0.0625)
  texts.stroke = round(windowHeight * 0.75 * 0.125)
}
